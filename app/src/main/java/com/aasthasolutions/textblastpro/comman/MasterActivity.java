package com.aasthasolutions.textblastpro.comman;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by My 7 on 10/6/2017.
 */
public class MasterActivity extends AppCompatActivity
{
    public void saveRememberMe(boolean flag)
    {
        SharedPreferences sp = getSharedPreferences("REMEMBER",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("REMEMBER_ME", flag);
        spe.commit();
    }

    public boolean getRemember()
    {
        SharedPreferences sp = getSharedPreferences("REMEMBER",MODE_PRIVATE);
        boolean flag = sp.getBoolean("REMEMBER_ME", false);
        return flag;
    }

    public void saveLoginDetail(String id, String pwd)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("ID", id);
        spe.putString("PWD",pwd);
        spe.commit();
    }

    public String getLoginId()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail",MODE_PRIVATE);
        String id = sp.getString("ID", "");
        return id;
    }

    public String getLoginPwd()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail",MODE_PRIVATE);
        String pwd = sp.getString("PWD","");
        return pwd;
    }

    public void opedialog(String title, String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MasterActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    public void openDialog(String value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MasterActivity.this);
        alertDialogBuilder.setTitle("Update Application");
        alertDialogBuilder.setMessage(value).setCancelable(false)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        }
                        catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                }).show();
    }

    public String getVersionInfo() {
        String versionName = "";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionName;
    }
}
