package com.aasthasolutions.textblastpro.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.aasthasolutions.textblastpro.R;
import com.aasthasolutions.textblastpro.comman.MasterActivity;
import com.aasthasolutions.textblastpro.utils.Api;
import com.aasthasolutions.textblastpro.utils.CustomRequest;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.yinglan.keyboard.HideUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends MasterActivity
{
    EditText editConsultantNumber,editPassword;

    CheckBox checkbox;

    RelativeLayout btnLogin,btnBack;

    public static ProgressDialog progressDialog;

    public static ArrayList<HashMap<String, String>> profiledetail;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        HideUtil.init(this);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Please Wait...!");
        progressDialog.setCancelable(false);

        initView();

        if(getRemember())
        {
            checkbox.setChecked(true);
            editConsultantNumber.setText(getLoginId());
            editPassword.setText(getLoginPwd());
        }
    }

    private void initView()
    {
        editConsultantNumber = (EditText) findViewById(R.id.editConsultantNumber);
        editPassword = (EditText) findViewById(R.id.editPassword);

        checkbox = (CheckBox) findViewById(R.id.checkbox);

        btnLogin = (RelativeLayout) findViewById(R.id.btnLogin);
        btnBack = (RelativeLayout) findViewById(R.id.btnBack);

        initClickEvent();
    }

    private void initClickEvent()
    {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editConsultantNumber.getText().toString().equalsIgnoreCase(getLoginId()) ||
                        !editPassword.getText().toString().equalsIgnoreCase(getLoginPwd()) && getRemember())
                {
                    saveLoginDetail(editConsultantNumber.getText().toString(),editPassword.getText().toString());
                }
                loginApi();
            }
        });

        checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    saveRememberMe(true);
                    saveLoginDetail(editConsultantNumber.getText().toString(),editPassword.getText().toString());
                }
                else
                {
                    saveRememberMe(false);
                }
            }
        });
    }

    private void loginApi()
    {
        if (CustomRequest.hasConnection(this))
        {
            progressDialog.show();

            String api = Api.baseurl + Api.login;

            Map<String, String> params = new HashMap<>();
            params.put("loginid", editConsultantNumber.getText().toString());
            params.put("password", editPassword.getText().toString());
            params.put("app", "a");
            params.put("version", getVersionInfo());

            Log.d("Params",params.toString());

            CustomRequest customRequest = new CustomRequest(Request.Method.POST, api, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("RESPONSE", "Login Response==>" + response.toString());
                    try {
                        progressDialog.dismiss();

                        if(response.has("status") && response.getString("status").equals("1"))
                        {
                            JSONObject jsonObject = response.getJSONObject("data");

                            profiledetail = new ArrayList<HashMap<String, String>>();

                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put("IdClient", jsonObject.getString("id_client"));
                            map.put("Name", jsonObject.getString("name"));
                            map.put("CellPhoneNumber", jsonObject.getString("cell_phone_number"));
                            map.put("EmailAddress", jsonObject.getString("email_address"));
                            map.put("Address", jsonObject.getString("address"));
                            map.put("CompanyCity", jsonObject.getString("company_city"));
                            map.put("State", jsonObject.getString("state"));
                            map.put("Zip", jsonObject.getString("zip"));
                            map.put("website", jsonObject.getString("website"));
                            map.put("facebook", jsonObject.getString("facebook"));
                            map.put("referred", jsonObject.getString("referred"));
                            map.put("routing", jsonObject.getString("routing"));
                            map.put("account_no", jsonObject.getString("account_no"));
                            map.put("admin_email", jsonObject.getString("admin_email"));
                            map.put("admin_phone_number", jsonObject.getString("admin_phone_number"));
                            map.put("ftp_host", jsonObject.getString("ftp_host"));
                            map.put("ftp_user", jsonObject.getString("ftp_user"));
                            map.put("ftp_password", jsonObject.getString("ftp_password"));
                            map.put("ftp_file_upload_path", jsonObject.getString("ftp_file_upload_path"));

                            profiledetail.add(map);

                            Log.e("RESPONCEEEEEE", "==> " + profiledetail);

                            progressDialog.dismiss();

                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            finish();
                        }
                        else if(response.getString("update").equalsIgnoreCase("1"))
                        {
                            openDialog(response.getString("message"));
                        }
                        else if(response.getString("update").equalsIgnoreCase("0")){
                            opedialog("Failed", response.getString("message"));
                        }
                        else {
                            opedialog("Failed", response.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.i("ERROR", "count==>" + error.toString());
                    progressDialog.dismiss();
                }
            }) {
            };
            customRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(customRequest);
        } else {
            opedialog("Connection Problem", "No Internet Connection !");
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(LoginActivity.this);
        myAlertDialog.setMessage("Are you sure you want to exit?");
        myAlertDialog.setTitle(R.string.app_name);
        myAlertDialog.setIcon(R.drawable.icon);
        myAlertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                finish();
                System.exit(0);
            }
        });
        myAlertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        myAlertDialog.show();
    }
}
