package com.aasthasolutions.textblastpro.activities;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.aasthasolutions.textblastpro.R;
import com.aasthasolutions.textblastpro.comman.MasterActivity;
import com.yinglan.keyboard.HideUtil;

/**
 * Created by My 7 on 17-Sep-18.
 */

public class ProfileActivity extends MasterActivity
{
    TextView lblNote,lblName,lblEmail,lblCompanyCity,lblAddress,lblCity,lblState,lblZip,lblPhone,lblCheckingAccount,lblRoutingNumber
             ,lblFacebook,lblReferedBy;

    EditText txtName,txtEmail,txtCompanyCity,txtAddress,txtCity,txtState,txtZip,txtPhone,txtCheckingAccount,txtRoutingNumber,
            txtFecebook,txtReferedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_profile);

        HideUtil.init(this);

        initView();
    }

    private void initView()
    {
        lblNote = findViewById(R.id.lblNote);
        lblName = findViewById(R.id.lblName);
        lblEmail = findViewById(R.id.lblEmail);
        lblCompanyCity = findViewById(R.id.lblCompanyCity);
        lblAddress = findViewById(R.id.lblAddress);
        lblCity = findViewById(R.id.lblCity);
        lblState = findViewById(R.id.lblState);
        lblZip = findViewById(R.id.lblZip);
        lblPhone = findViewById(R.id.lblPhone);
        lblCheckingAccount = findViewById(R.id.lblCheckingAccount);
        lblRoutingNumber = findViewById(R.id.lblRoutingNumber);
        lblFacebook = findViewById(R.id.lblFacebook);
        lblReferedBy = findViewById(R.id.lblReferedBy);

        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        txtCompanyCity = findViewById(R.id.txtCompanyCity);
        txtAddress = findViewById(R.id.txtAddress);
        txtCity = findViewById(R.id.txtCity);
        txtState = findViewById(R.id.txtState);
        txtZip = findViewById(R.id.txtZip);
        txtPhone = findViewById(R.id.txtPhone);
        txtCheckingAccount = findViewById(R.id.txtCheckingAccount);
        txtRoutingNumber = findViewById(R.id.txtRoutingNumber);
        txtFecebook = findViewById(R.id.txtFecebook);
        txtReferedBy = findViewById(R.id.txtReferedBy);
    }
}
